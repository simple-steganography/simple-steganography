var gsteganographyBundle = Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService);
var mystrings = gsteganographyBundle.createBundle("chrome://simplesteganography/locale/translations.properties");

function onLoad(){
	textField = document.getElementById('textMessage');
	labelField = document.getElementById('labelMessage');
	command = window.arguments[0];
	
	if(command == 'show')
	{
		textField.value = window.arguments[2]['textMessage'];
		labelField.value = mystrings.GetStringFromName("thisIsYourSecretMessage");
	} 
	else
	{
		textField.value = '';
		labelField.value = mystrings.formatStringFromName("pleaseEnterUpTo", [window.arguments[1]],1);
	}
}

function doOK(){
	window.arguments[2]['textMessage'] = document.getElementById('textMessage').value;
	return true;
}

