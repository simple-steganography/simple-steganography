var gsteganographyBundle = Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService);
var mystrings = gsteganographyBundle.createBundle("chrome://simplesteganography/locale/translations.properties");

var capacity;
var capacitySpaces = 0;
var capacitySorting = 0;
var capacityBoth = 0;


function onLoad(){
	textField = document.getElementById('textMessage');
	labelField = document.getElementById('labelMessage');
	capacityField = document.getElementById('labelCapacity');

	capacitySpaces = window.arguments[1];
	capacitySorting = window.arguments[2];
	capacityBoth = capacitySpaces + capacitySorting;
	capacity = capacitySorting;

	labelField.value = mystrings.formatStringFromName("pleaseEnterUpTo", [capacity],1);
	textField.value = '';
	updateCapacityInfo();
}

function selectMethod(){
	if(document.getElementById('rdoSort').selected){
		labelField.value = mystrings.formatStringFromName("pleaseEnterUpTo", [capacitySorting],1);
		capacity = capacitySorting;
	}else if(document.getElementById('rdoSpaces').selected){
		labelField.value = mystrings.formatStringFromName("pleaseEnterUpTo", [capacitySpaces],1);
		capacity = capacitySpaces;
	}else{
		labelField.value = mystrings.formatStringFromName("pleaseEnterUpTo", [capacityBoth],1);
		capacity = capacityBoth;
	}
	updateCapacityInfo();
}

function updateCapacityInfo(){
	capacityField.value = textField.value.length + ' (' + capacity + ')';
}

function doOK(){
	var method;
	if(document.getElementById('rdoSort').selected){
		method = 'sort';
	}else if(document.getElementById('rdoSpaces').selected){
		method = 'spaces';
	}else{
		method = 'both';
	}

	window.arguments[3]['embeddingMethod'] = method;
	window.arguments[3]['textMessage'] = document.getElementById('textMessage').value;
	return true;
}

