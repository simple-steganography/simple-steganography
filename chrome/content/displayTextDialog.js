var windowArguments;

function onLoad(){
	windowArguments = window.arguments[0].split('\n');
	field = document.getElementById('list');
	field.value = window.arguments[0];
}

function doOK(){
	return true;
}

function makeLI(){
	var text = new Array();
	for(n=0; n < windowArguments.length; n++) {
		text[n] = '<LI>' + windowArguments[n] + '</LI>';
	}
	
	field = document.getElementById('list');
	field.value = text.join('\n');
}

function makeP(){
	var text = new Array();
	for(n=0; n < windowArguments.length; n++) {
		text[n] = '<P>' + windowArguments[n] + '</P>';
	}
	
	field = document.getElementById('list');
	field.value = text.join('\n');
}


function makeWikiList(){
	var text = new Array();
	for(n=0; n < windowArguments.length; n++) {
		text[n] = '* ' + windowArguments[n];
	}
	
	field = document.getElementById('list');
	field.value = text.join('\n');
}

function removeFormatting() {
	field = document.getElementById('list');
	field.value = window.arguments[0];
}
